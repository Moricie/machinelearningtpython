import numpy as np, tensorflow as tf

traind, testd, trainl, testl = np.load(open("mnist.npz", "rb")).values()

# a)
sample1000 = testd[999, :, :]
class1000 = tf.arg_max(testl[999, :], 0)


s = tf.Session()

print("\nExercise8\n1\na) ", s.run(class1000))


# b)
minclass = tf.constant(0)
maxclass = tf.size(testl[0, :])-1

print("\nb) ", s.run(minclass), ", ", s.run(maxclass))


#c)
noclass9 = tf.math.reduce_sum(testl[:, 9], axis=0)

print("\nc) ", s.run(noclass9))


#d)
sample10 = tf.compat.v1.placeholder(tf.float32, [28, 28])
feedd = {sample10 : testd[9, :, :]}
sample10row = tf.reshape(sample10, [1, 28*28])
min10 = tf.math.reduce_min(sample10row)
max10 = tf.math.reduce_max(sample10row)

print("\nd)  min: ", s.run(min10, feedd), " max: ", s.run(max10, feedd))


#e)
sample10e2ndrow = sample10[::2, :]
sample10e2ndcol = sample10[:, ::2]
sample10invertall = sample10[::-1, ::-1]

col_to_zero = [0, 1, 2, 3, 4 ,5, 6, 7, 8, 9] # column numbers to be zeroed out
tnsr_shape = tf.shape(sample10)
mask = [tf.one_hot(col_num*tf.ones((tnsr_shape[0], ), dtype=tf.int32), tnsr_shape[-1])
        for col_num in col_to_zero]
mask = tf.reduce_sum(mask, axis=0)
mask = tf.cast(tf.logical_not(tf.cast(mask, tf.bool)), tf.float32)

print("\ne)\n2ndrow: ", s.run(tf.shape(sample10e2ndrow)), "\n2ndcol: ", s.run(tf.shape(sample10e2ndcol)), "\ninverted: ", s.run(tf.shape(sample10invertall)))



#with placeholder

feed1 = tf.placeholder(tf.float32, [10000]) 
result = tf.reduce_sum(feed1, axis=0)

npRes = s.run(result, feed_dict = { feed1 : testl[:,9] } )
print (npRes)

