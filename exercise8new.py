import numpy as np
import tensorflow as tf

traind, testd, trainl, testl = np.load(open("mnist.npz", "rb")).values()

tf.compat.v1.disable_eager_execution()

sess = tf.compat.v1.Session()

sample1000 = tf.compat.v1.placeholder(tf.float32, [28, 28], name="sample1000")
sample1000CV = tf.compat.v1.placeholder(tf.float32, [10], name="sample1000CV")
feed = {sample1000 : testd[999, :, :],
        sample1000CV : testl[999, :]}

shape = tf.shape(sample1000)
class1000 = tf.math.argmax(sample1000CV)

print("\n1\na)")
print("\n", sess.run([shape, class1000], feed))


class9 = tf.compat.v1.placeholder(tf.float32, [10000], name="class9")
feed = {class9 : testl[:, 9]}

mask = tf.equal(class9, 1)

noclass9 = tf.math.reduce_sum(tf.boolean_mask(class9, mask), axis=0)

print(sess.run(noclass9, feed))


sample10 = tf.compat.v1.placeholder(tf.float32, [28, 28], name="sample10")
feed = {sample10 : testd[9,:,:]}

high10 = tf.math.reduce_min(tf.math.reduce_min(sample10, axis=0), axis=0)
low10 = tf.math.reduce_max(tf.math.reduce_max(sample10, axis=0), axis=0)
returns = [high10, low10]

print("\n\nb)\n", sess.run(returns, feed))
print(sess.run)

sess.close()