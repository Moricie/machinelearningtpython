import numpy as np, tensorflow as tf ;

traind,testd,trainl,testl = np.load("mnist.npz").values() ;

data = tf.placeholder(tf.float64, shape=[None,28,28]) ;
targets = tf.placeholder(tf.float64, shape=[None,10]) ;

A0 = tf.reshape(data,[-1,784]) ;


w1 = tf.Variable((1.0), name="W1") ;
w2 = tf.Variable((3.0), name="W2") ;
loss = w1**2 + 2 * w2**2 ;

optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.1) ;
gd0p = optimizer.minimize(loss) ;

sess = tf.Session() ;
sess.run(tf.global_variables_initializer())

for it in range(0,20):
  sess.run(gd0p) ;
  lossValue = sess.run(tf.reduce_mean(loss)) ;
  print("It=", it, "loss=", lossValue) ;
