import numpy as np
import matplotlib.pyplot as plt

traind, testd, trainl, testl = np.load(open("mnist.npz", "rb")).values()

#2a average image over all training samples
fig,ax = plt.subplots(2,6)
avg = traind.mean(axis=0)
ax[0][0].imshow(avg)


#2b per pixel variance over all training samples
var = ((traind - avg)**2).mean(axis=0)
ax[0][1].imshow(var)


#2c per pixel standard deviation
std = np.sqrt(var)
ax[0][2].imshow(std)


#2d Slice out first 500 samples plot histogram of 13,13 and 0,0
first500 = traind[0:500, :, :]
ax[0][3].hist(first500[:, 13, 13])
ax[1][3].hist(first500[:, 0, 0])


#2e per sample min and max values and average over those
row_traind = np.reshape(traind, (60000, 784))

#min
allmin = row_traind.min(axis=1)
print("2e min average")
print(allmin.mean(axis=0))

#max
print("2e max average")
allmax = row_traind.max(axis=1)
print(allmax.mean(axis=0))


#2f
max_image = traind.max(axis=0)
ax[0][5].imshow(max_image)


#3a
s500 = traind[500,:,:] 
# both convert from one dimensional Array to row vector
# faster alternative
# s500 = np.ravel(s500).reshape(1, -1)
s500 = np.ravel(s500)
s500 = s500[np.newaxis, :]
print(s500.shape)
c500 = s500.transpose().dot(s500)
print (c500.shape)


#3b
batch1000 = traind[0:1000].reshape(-1, 28*28)
C = (batch1000[:, np.newaxis, :] * batch1000[:, :, np.newaxis]).mean(axis=0)
print ("C shape=", C.shape)


#3c
eigvals, eigvecs = np.linalg.eigh(C)
print(eigvals.shape)
print(eigvecs.shape)
eigv0 = eigvecs[:, -1].reshape(28,28)
eigv1 = eigvecs[:, -2].reshape(28,28)
eigv2 = eigvecs[:, -3].reshape(28,28)
fig,ax1=plt.subplots(1,3)
ax1[0].set_title("3c")
ax1[0].imshow(eigv0)
ax1[1].imshow(eigv1)


proj = np.dot(batch1000, eigvecs)
print(proj.shape)
proj[:, 0: -350] = 0
reproj = np.dot(proj, eigvecs.T)

visImg = reproj[10].reshape(28, 28)
ax1[2].imshow(visImg)

#end
plt.show()