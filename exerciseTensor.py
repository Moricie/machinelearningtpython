import numpy as np
import tensorflow as tf

traind, testd, trainl, testl = np.load(open("mnist.npz", "rb")).values()

tf.compat.v1.disable_eager_execution()

sess = tf.Session()

sample_1000 = tf.placeholder(tf.float32, [28, 28], name="sample_1000")
class_1000 = tf.placeholder(tf.float32, [10], name="class_1000")

feed = {sample_1000 : testd[999,:,:],
        class_1000 : testl[999, :]}

shape = tf.shape(sample_1000)
classno = tf.arg_max(class_1000, dimension=0)

print(sess.run([shape, classno], feed_dict=feed))



class_9 = tf.placeholder(tf.float32, [None])

feed = {class_9 : testl[:, 9]}

mask = tf.equal(class_9, 1)

no_class_9 = tf.reduce_sum(tf.boolean_mask(class_9, mask), axis=0)

output = [no_class_9]

print(sess.run(output, feed_dict=feed))



sample_10 = tf.placeholder(tf.float32, [28, 28], name="sample_10")

feed = {sample_10 : testd[9, :, :]}

high_10 = tf.math.reduce_max(tf.reduce_max(sample_10, axis=0), axis=0)
low_10 = tf.math.reduce_min(tf.reduce_min(sample_10, axis=0), axis=0)

output = [high_10, low_10]

print(sess.run(output, feed))

rows2nd = tf.shape(sample_10[::2, :])
cols2nd = tf.shape(sample_10[:, ::2])
inverted = tf.shape(sample_10[::-1, ::-1])

ones = np.ones([1, 28], np.float32)

for i in range(0, 10):
    ones[0, i] = 0

for i in range(1, 11):
    ones[0, -i] = 0

print(ones)
ones = np.repeat(ones, 28, axis=0)
ones = np.reshape(ones, [28, 28])
print(ones)

sample10zeros = tf.matmul(sample_10, ones)
inverted2nd = tf.shape(sample_10[::-2, ::-2])

print(sess.run(sample10zeros, feed))

class_4 = tf.placeholder(tf.float32, [None], name="class_4")
class_9 = tf.placeholder(tf.float32, [None], name="class_9")

feed = {class_4 : testl[:, 4],
        class_9 : testl[:, 9]}

mask_4 = tf.equal(class_4, 1)

shape_mask = tf.shape(mask_4)

class4_samples = tf.boolean_mask(traind, mask_4, axis=0)
shape_class4 = tf.shape(class4_samples)

output = [shape_mask, shape_class4]

print(sess.run(output, feed))

mask_9 = tf.equal(class_9, 1)
combined_mask = tf.logical_or(mask_4, mask_9)

combined_samples = tf.boolean_mask(traind, combined_mask, axis=0)
shape_comb = tf.shape(combined_samples)

print(sess.run(shape_comb, feed))