import numpy as np
import numpy.random as npr

#a)
traind, testd, trainl, testl = np.load(open("mnist.npz", "rb")).values()

#b)
print(trainl[1000,:])

#c)
print(trainl[:,:].min(axis=0))

#d)
print(trainl[:,:].max(axis=0))

#e)
print(len(trainl[0,:]))

#e)
print(trainl[:,5].sum(axis=0))

#f)
sample10 = traind[10,:,:]
print(sample10.max(axis=0).max(), sample10.min(axis=0).min())
print(sample10[::2,:])
print(sample10[:,::2])
print(sample10[::-1, ::-1])
sample10_row_len = len(sample10[0,:])
sample10_num_rows = len(sample10[:,0])
sample10[:, sample10_row_len - 10: sample10_row_len] = np.zeros([sample10_num_rows, 10])
sample10[:, 0: 10] = np.zeros([sample10_num_rows, 10])
print(sample10[::-2,::-2])

#g)
class_vector = trainl.argmax(axis=1)
class_mask = (class_vector == 4)
print(class_mask)
traind[class_mask]

#h)
class_mask9 = (class_vector == 9)
print(class_mask9)
class_mask49 = np.logical_or(class_mask, class_mask9)
print(traind[class_mask49])

#i)
print(traind[0:10000, :, :])
print(trainl[0:10000, :])

#j)
indices = np.arange(0, traind.shape[0])
npr.shuffle(indices)
thousand_rand_inds = indices[0:1000]
print(traind[thousand_rand_inds, :, :])