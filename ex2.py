import numpy as np
import matplotlib.pyplot as plt

import random as rand

traind, testd, trainl,  testl = np.load(open("mnist.npz", "rb")).values()

fig, ax = plt.subplots(2, 5)


s1000 = traind[1000, :, :]
ax[0][0].set_title("Class: " + str(np.argmax(trainl[1000])))
ax[0][0].imshow(s1000)


class_vector = np.argmax(trainl, axis=1)

print("Highest class: ", class_vector.max())
print("Lowest class: ", class_vector.min())


mask = (trainl == 1)
#print(mask)
num_of_occ_vec = np.sum(trainl, axis=0)
print("Occurences of class 5: ", num_of_occ_vec[5])


s10 = traind[10]
print("Smallest pixel: ", np.min(s10), "Highest pixel: ", np.max(s10))


s10e2ndrow = s10[::2, :]
ax[0][1].set_title("s10 Every second row")
ax[0][1].imshow(s10e2ndrow)

s10e2ndcol = s10[:, ::2]
ax[1][1].set_title("s10 Every second column")
ax[1][1].imshow(s10e2ndcol)

s10inverse = s10[::-1, ::-1]
ax[0][2].set_title("s10 inversed")
ax[0][2].imshow(s10inverse)

s10f10eq0row = np.copy(s10)
s10f10eq0row[0:10, :] = 0
ax[0][3].set_title("First ten rows 0")
ax[0][3].imshow(s10f10eq0row)

s10f10eq0col = np.copy(s10)
s10f10eq0col[:, 0:10] = 0
ax[1][3].set_title("First ten cols 0")
ax[1][3].imshow(s10f10eq0col)

s10e2ndinverse = s10[::-2, ::-2]
ax[1][2].set_title("s10 Every second inversed")
ax[1][2].imshow(s10e2ndinverse)


class_vector = np.argmax(trainl, axis=1)
mask49 = np.logical_or(class_vector == 4, class_vector == 9)
class49 = traind[mask49]
print(class49.shape)

first10000 = traind[:10000, :, :]

transform = 1 - traind[:, :, :]
ax[1][0].set_title("1-x")
ax[1][0].imshow(transform[0, :, :])

'''
#Very slow
shuffled = traind.copy()

rand.shuffle(np.reshape(traind, -1, 28*28))

rand1000 = shuffled[:1000, :, :]
'''

#faster              0 to 60000
indices = np.arange(0, traind.shape[0])
rand.shuffle(indices)
thousand_rand_ints = indices[0:1000]

ax[0][4].imshow(traind[thousand_rand_ints[0], :, :])
ax[1][4].imshow(traind[thousand_rand_ints[1], :, :])


plt.show()