import sys
import os
import PIL.Image as Img
import numpy as np
import tensorflow as tf 
import gc

tf.compat.v1.disable_eager_execution()

# learning rate
learning_rate = 0.001

# hidden layer size L
L = 320

B = 400

EPOCHS = 50

# image directory path
path = sys.argv[1]

# image parameters
width = int(sys.argv[2])
height = int(sys.argv[3])
channels = int(sys.argv[4])

# read flattened image file contents into np array

imgObjs = []
for f in os.listdir(sys.argv[1]) :
    if f.find(".png") != -1:
        o = Img.open(path+"/"+f) ;
        o.load() ;
        imgObjs.append(np.array(o).ravel()) ;
X = np.array(imgObjs, dtype=np.float32) ;

print(X.shape)

# create labels from filenames
numLabels = numLabels = [int(f.split("_")[0].replace("class",""))
for f in os.listdir(path)] ;
T = np.zeros([len(numLabels),10]) ;
# fancy indexing for axes 0 and 1 !!!
T[range(0,len(numLabels)),numLabels] = 1 ;

X = X / 255 ;

'''
# PCA transform, we keep only K values per sample 
K = 20 ; 
# # covariance matrix 
C = (X[:,:,np.newaxis] * X_[:,np.newaxis,:]).mean(axis=0) ;
eigvals, eigvecs = np.linalg.eigh(C) ;
Xprime = np.dot(X, eigvecs) ;
X = Xprime[:,0:K] ;
'''

indices = np.arange(0,X.shape[0]) ;
np.random.shuffle(indices) ;
X = X[indices,:] ;
T = T[indices,:] ;

# 50-50 split of X into train and test data
nu = 0.5 ;
splitIndex = int(X.shape[0]*nu) ;
traind = X[0:splitIndex,:] ;
trainl = T[0:splitIndex] ;
testd = X[splitIndex:, :] ;
testl = T[splitIndex:] ;


X = tf.compat.v1.placeholder(tf.float32, [None,width * height]) ;
T = tf.compat.v1.placeholder(tf.float32, [None,10]) ;
W1 = tf.Variable(np.random.uniform(-0.01,0.01, (width * height,L)), dtype=tf.float32) ;
b1 = tf.Variable(np.random.uniform(-0.01,0.01, (1,L)), dtype=tf.float32) ;
W3 = tf.Variable(np.random.uniform(-0.01,0.01, (L,L)), dtype=tf.float32) ;
b3 = tf.Variable(np.random.uniform(-0.01,0.01, (1,L)), dtype=tf.float32) ;
W5 = tf.Variable(np.random.uniform(-0.01,0.01, (L,10)), dtype=tf.float32) ;
b5 = tf.Variable(np.random.uniform(-0.01,0.01, (1,10)), dtype=tf.float32) ;
#W7 = tf.Variable(np.random.uniform(-0.01,0.01, (L,10)), dtype=tf.float32) ;
#b7 = tf.Variable(np.random.uniform(-0.01,0.01, (1,10)), dtype=tf.float32) ;





A1 = tf.matmul(X, W1) + b1 ;
A2 = tf.nn.relu(A1) ;
A3 = tf.matmul(A2, W3) + b3 ;
A4 = tf.nn.relu(A3) ;
A5 = tf.matmul(A4, W5) + b5 ;
#A6 = tf.nn.relu(A5) ;
#A7 = tf.matmul(A6, W7) + b7 ;
loss = tf.compat.v1.losses.mean_squared_error(T, A5) ;




# often better: AdamOptimizer
gdObj = tf.compat.v1.train.AdamOptimizer(learning_rate=learning_rate, epsilon=1e-5) ;
#gdObj = tf.compat.v1.train.SGDOptimizer(learning_rate=learning_rate) ;
updateOp = gdObj.minimize(loss) ;






with tf.compat.v1.Session() as sess:
    
    sess.run(tf.compat.v1.global_variables_initializer())
    

    ep = 1
    for iter in range(0,EPOCHS):
        n = 0 ;
        nrBatches = traind.shape[0] / B ;
        out = ""
        for iter in range(0,int(nrBatches)):
            batch_n_data = traind[n*B:(n+1)*B,:] ;
            batch_n_labels= trainl[n*B:(n+1)*B,:] ;
            n += 1 ;
            if n > nrBatches: n = 0 ;
            # assume A7 to be the last DNN/CNN layer
            feed_dict = {X : batch_n_data,
                            T : batch_n_labels}
            sess.run(updateOp, feed_dict=feed_dict) ;
            ce = tf.math.reduce_mean(loss) ;
            ce_np = sess.run(fetches=ce, feed_dict=feed_dict) ;
            out = ce_np ;
        print("EP: " + str(ep) + " Loss: " + str(out)) ;
        ep+=1 ;

        gc.collect()




    feed_dict = {X : testd, 
                 T : testl}

    # assume A5 to be the last DNN/CNN layer
    num_labels = tf.argmax(T, axis = 1) ;
    num_logits = tf.argmax(A5, axis = 1 ) ;
    eq = tf.equal(num_labels,num_logits) ;
    cast = tf.cast(eq, tf.float32) ;
    test = tf.math.reduce_mean(cast) ;
    # important to feed whole test data/labels !
    ce_np = sess.run(test, feed_dict = feed_dict) ;
    print("test:" + str(ce_np)) ;