import sys
import os
import PIL.Image as Img
import numpy as np
import tensorflow as tf 
import gc

tf.compat.v1.disable_eager_execution()

# learning rate epsilon
learning_rate = 0.001

# hidden layer size L
L = 200

B = 500

EPOCHS = 50

# image directory path
path = sys.argv[1]

# image parameters
width = int(sys.argv[2])
height = int(sys.argv[3])
channels = int(sys.argv[4])

# read flattened image file contents into np array

imgObjs = []
for f in os.listdir(sys.argv[1]) :
    if f.find(".png") != -1:
        o = Img.open(path+"/"+f) ;
        o.load() ;
        imgObjs.append(np.array(o)) ;
X = np.array(imgObjs, dtype=np.float32) ;

print(X.shape)

# create labels from filenames
numLabels = numLabels = [int(f.split("_")[0].replace("class",""))
for f in os.listdir(path)] ;
T = np.zeros([len(numLabels),10]) ;
# fancy indexing for axes 0 and 1 !!!
T[range(0,len(numLabels)),numLabels] = 1 ;

X = X / 255 ;


indices = np.arange(0,X.shape[0]) ;
np.random.shuffle(indices) ;
X = X[indices,:] ;
T = T[indices,:] ;

# 50-50 split of X into train and test data
nu = 0.5 ;
splitIndex = int(X.shape[0]*nu) ;
traind = X[0:splitIndex,:] ;
trainl = T[0:splitIndex] ;
testd = X[splitIndex:, :] ;
testl = T[splitIndex:] ;


X = tf.compat.v1.placeholder(tf.float32,[None,width * height]) ;
T = tf.compat.v1.placeholder(tf.float32,[None,10]) ;
X = tf.compat.v1.reshape(X,(-1,width,height,channels)) ;
conv1 = tf.nn.relu(tf.compat.v1.layers.conv2d(X, 6, 5,name="C1")) ;
pool1 = tf.compat.v1.layers.max_pooling2d(conv1,2,2) ;
conv2 = tf.nn.relu(tf.compat.v1.layers.conv2d(X, 6, 5,name="C2")) ;
pool2 = tf.compat.v1.layers.max_pooling2d(conv1,2,2) ;
flatDims = 10 ; # compute from filters etc.
flatDNNLayer = tf.reshape(pool2,(-1,flatDims)) ;
# from here on like DNN classifier

loss = tf.compat.v1.losses.mean_squared_error(T, flatDNNLayer) ;

# often better: AdamOptimizer
gdObj = tf.compat.v1.train.AdamOptimizer(learning_rate=learning_rate, epsilon=1e-6) ;
#gdObj = tf.compat.v1.train.SGDOptimizer(learning_rate=learning_rate) ;
updateOp = gdObj.minimize(loss) ;






with tf.compat.v1.Session() as sess:
    
    sess.run(tf.compat.v1.global_variables_initializer())
    


    for iter in range(0,EPOCHS):
        n = 0 ;
        nrBatches = traind.shape[0] / B ;
        for iter in range(0,int(nrBatches)):
            batch_n_data = traind[n*B:(n+1)*B,:,:,tf.newaxis] ;
            batch_n_labels= trainl[n*B:(n+1)*B,:] ;
            n += 1 ;
            if n > nrBatches: n = 0 ;
            # assume A3 to be the last DNN/CNN layer
            feed_dict = {X : batch_n_data,
                            T : batch_n_labels}
            sess.run(updateOp, feed_dict=feed_dict) ;
            ce = tf.math.reduce_mean(loss) ;
            ce_np = sess.run(fetches=ce, feed_dict=feed_dict) ;
            print(ce_np)

        gc.collect()




    feed_dict = {X : testd, 
                T : testl}

    # assume A3 to be the last DNN/CNN layer
    num_labels = tf.argmax(T, axis = 1) ;
    num_logits = tf.argmax(flatDNNLayer, axis = 1 ) ;
    eq = tf.equal(num_labels,num_logits) ;
    cast = tf.cast(eq, tf.float32) ;
    test = tf.math.reduce_mean(cast) ;
    # important to feed whole test data/labels !
    ce_np = sess.run(test, feed_dict = feed_dict) ;
    print("test:" + str(ce_np)) ;