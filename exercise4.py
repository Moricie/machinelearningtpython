import numpy as np
import matplotlib.pyplot as plt

input = np.array([1.0, 3.0])
STEP_SIZE = 0.1
NO_OF_STEPS = 10


def f(arr):
    x1 = arr[0]
    x2 = arr[1]
    print(x1**2 + 2 * x2**2)


def grad_of_f(arr):
    return np.array([2., 4.]) * arr


def gradient_descent_of_f(arr):
    
    return arr - (0.1 * grad_of_f(arr))

print(input)
i = 0


for iterator in range(0, 10):
    input = input - (0.1 * grad_of_f(input))
    print(input)

'''
while (i < NO_OF_STEPS):
    input = gradient_descent_of_f(input)
    print(input)
    i += 1
'''

#4

traind, testd, trainl, testl = np.load(open("mnist.npz", "rb")).values()


batch500 = traind[0:500, :, :].reshape(-1, 784)
C = (batch500[:, np.newaxis, :] * batch500[:, :, np.newaxis]).mean(axis=0)
print("covariance matrix shape = ", C.shape)

eigenvals, eigenvecs = np.linalg.eigh(C)

#last
last5 = eigenvecs[:, 0:5].reshape(28, 28, -1)
first5 = eigenvecs[:, -5:].reshape(28, 28, -1)

print(last5.shape, first5.shape)

fig, ax = plt.subplots(3,5)
ax[0, 0].imshow(first5[:, :, 0])
ax[0, 1].imshow(first5[:, :, 1])
ax[0, 2].imshow(first5[:, :, 2])
ax[0, 3].imshow(first5[:, :, 3])
ax[0, 4].imshow(first5[:, :, 4])
ax[1, 0].imshow(last5[:, :, 0])
ax[1, 1].imshow(last5[:, :, 1])
ax[1, 2].imshow(last5[:, :, 2])
ax[1, 3].imshow(last5[:, :, 3])
ax[1, 4].imshow(last5[:, :, 4])


#5

proj = np.dot(batch500, eigenvecs)

print(proj.shape)

KEEP = 100

proj[:, 0:-KEEP] = 0

reproj = np.dot(proj, eigenvecs.T)

imgs = reproj[0:5,:]. reshape(-1, 28, 28)

for it in range(5):
    ax[2, it].imshow(imgs[it])

plt.show()
