import numpy as np
import matplotlib.pyplot as plt


# 1 ReLU layer
def relu(X):
    mask = (X >= 0)
    return X * mask

print("1 ReLU transformation")
print(relu(np.array([1, 0, -1])))
print(relu(np.array([1, 0, 10])))
print(relu(np.array([-1, 0, -10])))


# 2 affine layer
def affine(X, W, b):
    return np.dot(X, W) + b


W = np.array([[2, 0, 0], [0, 2, 0]])
b = np.array([1, -1, 0])

print("\n\n2 Affine layer transformation")
print(affine(np.array([[1, 1], [0, -1]]), W, b), "\n")
print(affine(np.array([[1, 0], [1, -1]]), W, b), "\n")
print(affine(np.array([[1, 1], [1, 1]]), W, b), "\n")


# 3 DNN implementing
def batchS(X):
    xex = np.exp(X)
    return xex/(xex.sum(axis=1)[:,np.newaxis])

X = np.array([[1, 1], 
                [0, -1]])

W1 = W
b1 = np.array([1, -1, 0])

W3 = np.array([[4, 0], 
                [0, -1], 
                [0, -1]])
b3 = np.array([1, -1])

print("3 DNN")
print(batchS(affine(relu(affine(X, W1, b1)), W3, b3)))

X1 = affine(X, W1, b1)
X2 = relu(X1)
X3 = affine(X2, W3, b3)
print("X3 ")
print(X3)
