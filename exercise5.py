import numpy as np
import matplotlib.pyplot as plt

#exercise 5

#3 Single Sample Softmax
print()
print("Single Sample Softmax")
def S(x):
    xex = np.exp(x - np.max(x))
    xex /= xex.sum()
    return xex

vals0 = [-1, -1, 5]
vals1 = [1, 1, 2]
print(S(vals0))
print(S(vals1))

#4 single sample cross-entropy
#x = predictions, t = targets
print()
print("Single sample Cross Entropy")
def CE(x, t):
    return -np.log(x[np.argmax(t)])

t = [0, 0, 1]
x0 = [0.1, 0.1, 0.8]
x1 = [0.3, 0.3, 0.4]
x2 = [0.8, 0.1, 0.1]

print(CE(x0, t))
print(CE(x1, t))
print(CE(x2, t))

#Multi Sample Softmax
print()
print("Multi sample Softmax")

def batchS(x):
    xex = np.exp(x)

    return xex/(xex.sum(axis=1)[:,np.newaxis])

def batchCE(x, t):
    print(x.shape)
    return (-(t*np.log(x)).sum(axis=1).mean())

x0 = [[-1, -1, 5],
    [-1, -1, 5],
    [-1, -1, 5]]
x1 = [[1, 1, 2],
    [1, 1, 2],
    [1, 1, 2]]

print(batchS(x0))

x000 = np.array([[-1, -1, 5],
    [-1, -1, 5],
    [-1, -1, 5]])
tx3 = np.array([t, t, t])

print(batchCE(x000, tx3))


# Ex 4
# Ex. 3

# construct array with the two samples as rows
X1 = np.array([[-1,-1,5],[1,1,2]]) 

print("softmax 0", S(X1[0,:])) 
print("softmax 1",S(X1[1,:])) 

T2 =  np.array([[0,0,1],[0,0,1],[ 0,0,1]]) 

X2 = np.array([[0.1,0.1,0.8],[0.3,0.3,0.4],[ 0.8,0.1,0.1]]) 

print("CE1",CE(X2[0,:], T2[0])) 
print("CE2",CE(X2[1,:],T2[1])) 
print("CE3",CE(X2[2,:],T2[2])) 

# Ex. 5
print("batch softmax", batchS(X1)) 
print("batch CE", batchCE(X2,T2)) 